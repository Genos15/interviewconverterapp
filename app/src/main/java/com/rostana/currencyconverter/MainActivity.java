package com.rostana.currencyconverter;


import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.rostana.currencyconverter.Config.Logger;
import com.rostana.currencyconverter.Format.ValuteAdapter;
import com.rostana.currencyconverter.IO.FetcherApi;
import com.rostana.currencyconverter.Repository.OperationType;
import com.rostana.currencyconverter.State.ApplicationState;
import com.rostana.currencyconverter.State.Identifier.Identifier;
import com.rostana.currencyconverter.ViewModel.ValuteViewModel;

import java.util.Objects;

public class MainActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    private TextView text_selected_item;
    private RecyclerView recyclerView;
    private ValuteAdapter valuteAdapter;
    private MenuItem globalSearchBar;
    private BottomSheetDialog bottomSheet;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        text_selected_item = findViewById(R.id.text_currency);
        EditText text_input_data = findViewById(R.id.input_data);
        text_input_data.setHint("~1");

        ApplicationState.getInstance().Init(this);
        ApplicationState.getInstance().setState(Identifier.VALUE_INPUT, 1);


        recyclerView = findViewById(R.id.recycler_valute);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        int resId = R.anim.layout_animation_fall_down;
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(this, resId);
        recyclerView.setLayoutAnimation(animation);

        valuteAdapter = new ValuteAdapter();
        recyclerView.setAdapter(valuteAdapter);

        valuteAdapter.setOnValuteItemClickListener(position -> {
            if (position != RecyclerView.NO_POSITION) {
                if (recyclerView.findViewHolderForAdapterPosition(position) != null) {
                    final String text_value = ((TextView)
                            Objects.requireNonNull(
                                    recyclerView.findViewHolderForAdapterPosition(position)).
                                    itemView.
                                    findViewById(R.id.text_charcode)).getText().toString();
                    ApplicationState.getInstance().setState(Identifier.ACTIVE_CURRENCY, text_value);
                    text_selected_item.setText(text_value);
                }
            }
        });

        ValuteViewModel valuteViewModel = ViewModelProviders.of(this).get(ValuteViewModel.class);
        valuteViewModel.getAllValutes().observe(this, valuteAdapter::setValutes);


        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == 0) hideKeyboard(MainActivity.this);
            }
        });


        Button button_done = findViewById(R.id.button_done);
        button_done.setOnClickListener(view -> {
            hideKeyboard(MainActivity.this);
            if (globalSearchBar.isActionViewExpanded()) globalSearchBar.collapseActionView();
            double value_input;
            if (text_input_data.getText().toString().isEmpty()) value_input = 1d;
            else value_input = Double.valueOf(text_input_data.getText().toString());
            ApplicationState.getInstance().setState(Identifier.VALUE_INPUT, value_input);
            valuteAdapter.notifyDataSetChanged();
        });

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        text_selected_item.setOnClickListener(v -> {
            if (globalSearchBar != null) {
                if (globalSearchBar.isActionViewExpanded()) {
                    globalSearchBar.collapseActionView();
                    text_input_data.setHint("~1");
                } else {
                    globalSearchBar.expandActionView();
                    text_input_data.setHint("");
                }
            }
        });
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (!ApplicationState.getInstance().has(Identifier.SELECTED_VALUTE)) {
            ApplicationState.getInstance().setState(Identifier.SELECTED_VALUTE, 0f);
            bottomSheet = new BottomSheetDialog(this);
            final View bottomSheetLayout = getLayoutInflater().inflate(R.layout.bottom_sheet_dialog, null);
            (bottomSheetLayout.findViewById(R.id.button_ok)).setOnClickListener(v -> bottomSheet.dismiss());
            bottomSheet.setContentView(bottomSheetLayout);
            bottomSheet.show();
        }
        if (!ApplicationState.getInstance().has(Identifier.ACTIVE_CURRENCY)) {
            ApplicationState.getInstance().setState(Identifier.ACTIVE_CURRENCY, "");
        } else {
            text_selected_item.setText(ApplicationState.getInstance().get(Identifier.ACTIVE_CURRENCY).toString());
        }
    }


    //    private void runLayoutAnimation(final RecyclerView recyclerView) {
//        // Just in the case For Future if you give me the Job :)
//        final LayoutAnimationController controller =
//                AnimationUtils.loadLayoutAnimation(recyclerView.getContext(), R.anim.layout_animation_fall_down);
//        recyclerView.setLayoutAnimation(controller);
//        recyclerView.scheduleLayoutAnimation();
//    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            Logger.getInstance().Init(this);
            if (ApplicationState.getInstance().has(Identifier.IS_FIRST_LAUNCH))
                FetcherApi.getInstance().onFetch(getApplication(), OperationType.UPDATE);
            else
                FetcherApi.getInstance().onFetch(getApplication(), OperationType.INSERT);
        } catch (Exception e) {
            Logger.getInstance().push("Fetch Error", e.getMessage());
        } finally {
            if (recyclerView.findViewHolderForAdapterPosition(0) != null) {
                final String text_value = ((TextView)
                        Objects.requireNonNull(
                                recyclerView.
                                        findViewHolderForAdapterPosition(0)).
                                itemView.
                                findViewById(R.id.text_charcode)).getText().toString();
                text_selected_item.setText(text_value);
            }
        }

    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) view = new View(activity);
        assert imm != null;
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        MenuItem item = menu.findItem(R.id.action_edit);
        globalSearchBar = item;
        SearchView searchView = (SearchView) item.getActionView();
        searchView.setOnQueryTextListener(this);
        searchView.setQueryHint(getString(R.string.hint_searchView));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_edit) {
            hideKeyboard(MainActivity.this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        valuteAdapter.getFilter().filter(s);
        valuteAdapter.notifyDataSetChanged();
        return false;
    }
}
