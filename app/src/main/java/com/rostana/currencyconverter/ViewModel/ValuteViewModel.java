package com.rostana.currencyconverter.ViewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;
import com.rostana.currencyconverter.Model.Valute;
import com.rostana.currencyconverter.Repository.ValuteRepository;
import java.util.List;

public class ValuteViewModel extends AndroidViewModel {

    private ValuteRepository repository;
    private LiveData<List<Valute>> allValutes;

    public ValuteViewModel(@NonNull Application application) {
        super(application);

        repository =  new ValuteRepository(application);
        allValutes = repository.getAllValutes();
    }

    public void insert(Valute valute){
        repository.insert(valute);
    }

    public void update(Valute valute){
        repository.update(valute);
    }

    public void delete(Valute valute){
        repository.delete(valute);
    }

    public void deleteAll(){
        repository.deleteAll();
    }

    public LiveData<List<Valute>> getAllValutes(){
        return allValutes;
    }

}
