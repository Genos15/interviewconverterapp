package com.rostana.currencyconverter.Model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Entity(tableName = "valute_table")
@Root(name = "Valute")
public class Valute {
    @PrimaryKey()
    @Attribute(name = "ID")
    @NonNull
    private String ID;
    @Element(name = "NumCode")
    private String NumCode;
    @Element(name = "CharCode")
    private String CharCode;
    @Element(name = "Nominal")
    private String Nominal;
    @Element(data = true, name = "Name")
    private String Name;
    @Element(name = "Value")
    private String Value;
    private int Priority;
    private String PrintValue;


    public Valute(@NonNull String ID, String numCode, String charCode, String nominal, String name, String value) {
        this.ID = ID;
        NumCode = numCode;
        CharCode = charCode;
        Nominal = nominal;
        Name = name;
        Value = value;
        PrintValue ="";
        Priority = 0;
    }

    public Valute() {}

    public void setID(String id) {
        this.ID = id;
    }

    public String getID() {
        return ID;
    }

    public String getNumCode() {
        return NumCode;
    }

    public String getCharCode() {
        return CharCode;
    }

    public String getNominal() {
        return Nominal;
    }

    public String getValue() {
        return Value;
    }

    public void setNumCode(String numCode) {
        NumCode = numCode;
    }

    public void setCharCode(String charCode) {
        CharCode = charCode;
    }

    public void setNominal(String nominal) {
        Nominal = nominal;
    }

    public void setValue(String value) {
        Value = value;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public int getPriority() {
        return Priority;
    }

    public void setPriority(int priority) {
        this.Priority = priority;
    }

    public String getPrintValue() {
        return PrintValue;
    }

    public void setPrintValue(String printValue) {
        PrintValue = printValue;
    }

    @Override
    public String toString() {
        return "Valute{" +
                "ID='" + ID + '\'' +
                ", NumCode='" + NumCode + '\'' +
                ", CharCode='" + CharCode + '\'' +
                ", Nominal='" + Nominal + '\'' +
                ", Name='" + Name + '\'' +
                ", Value='" + Value + '\'' +
                ", Priority=" + Priority +
                ", PrintValue='" + PrintValue + '\'' +
                '}';
    }
}
