package com.rostana.currencyconverter.Model;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

@Root(name = "ValCurs")
public class ValCurs {

    @Attribute(name = "Date")
    private String Date;
    @Attribute(name = "name")
    private String name;
    @ElementList(inline = true)
    private List<Valute> valutes;

    public ValCurs() {}

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Valute> getValutes() {
        return valutes;
    }

    public void setValutes(List<Valute> valutes) {
        this.valutes = valutes;
    }

    @Override
    public String toString() {
        return "ValCurs{" +
                "Date='" + Date + '\'' +
                ", name='" + name + '\'' +
                ", valutes=" + valutes +
                '}';
    }
}
