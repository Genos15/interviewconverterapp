package com.rostana.currencyconverter.IO;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Xml;
import com.rostana.currencyconverter.Config.Logger;
import com.rostana.currencyconverter.Model.Valute;
import com.rostana.currencyconverter.Repository.OperationType;
import com.rostana.currencyconverter.Repository.ValuteRepository;
import com.rostana.currencyconverter.State.ApplicationState;
import com.rostana.currencyconverter.State.Identifier.Identifier;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


public class FetcherApi {
    private static final String API_URL = "http://www.cbr.ru/scripts/XML_daily.asp";
    private static final FetcherApi ourInstance = new FetcherApi();

    public static FetcherApi getInstance() {
        return ourInstance;
    }

    private FetcherApi() {}

    private ValuteRepository repository;

    public void onFetch(Application application, OperationType type){
        repository =  new ValuteRepository(application);
        new FetchAsyncTask(type).execute(API_URL);
    }


    private static synchronized List<Valute> getXML2FromUrl(final String url) {

        try {

            HttpURLConnection connection = (HttpURLConnection)(new URL(url)).openConnection();
            connection.setReadTimeout(10000 /* milliseconds */);
            connection.setConnectTimeout(15000 /* milliseconds */);
            connection.setRequestMethod("GET");
            connection.setDoInput(true);
            connection.connect();
            List<Valute> entries = parse(connection.getInputStream());
            return entries;
        } catch (Exception e) {
            e.printStackTrace();
            Logger.getInstance().push(e.getMessage());
            return null;
        }
    }


    public static List parse(InputStream in) throws XmlPullParserException, IOException {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            return readFeed(parser);
        } finally {
            in.close();
        }
    }



    private static List readFeed(XmlPullParser parser) throws XmlPullParserException, IOException {
        List entries = new ArrayList();

        parser.require(XmlPullParser.START_TAG, null, "ValCurs");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals("Valute")) {
                entries.add(readEntry(parser));
            } else {
                skip(parser);
            }
        }
        return entries;
    }


    private static Valute readEntry(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, null, "Valute");
        String _ID = "";
        String tag = parser.getName();
        if (tag.equals("Valute")) _ID = parser.getAttributeValue(null, "ID");
        String _NumCode = null;
        String _CharCode = null;
        String _Nominal = null;
        String _Name = null;
        String _Value = null;

        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals("NumCode")) {
                _NumCode = readNumCode(parser);
            } else if (name.equals("CharCode")) {
                _CharCode = readCharCode(parser);
            } else if (name.equals("Nominal")) {
                _Nominal = readNominal(parser);
            }else if (name.equals("Name")) {
                _Name = readName(parser);
            }else if (name.equals("Value")) {
                _Value = readValue(parser);
            }
            else {
                skip(parser);
            }
        }

        return new Valute(_ID, _NumCode, _CharCode, _Nominal, _Name, _Value);
    }


    private static String readNominal(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, null, "Nominal");
        String summary = readText(parser);
        parser.require(XmlPullParser.END_TAG, null, "Nominal");
        return summary;
    }

    // Processes summary tags in the feed.
    private static String readCharCode(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, null, "CharCode");
        String summary = readText(parser);
        parser.require(XmlPullParser.END_TAG, null, "CharCode");
        return summary;
    }


    private static String readNumCode(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, null, "NumCode");
        String title = readText(parser);
        parser.require(XmlPullParser.END_TAG, null, "NumCode");
        return title;
    }

    private static String readName(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, null, "Name");
        String title = readText(parser);
        parser.require(XmlPullParser.END_TAG, null, "Name");
        return title;
    }

    private static String readValue(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, null, "Value");
        String title = readText(parser);
        parser.require(XmlPullParser.END_TAG, null, "Value");
        return title;
    }


    private static String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }


    private static void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }

    private static class FetchAsyncTask extends AsyncTask<String, Void, Void>{
        private OperationType type;
        FetchAsyncTask(OperationType _type){
            type = _type;
        }

        @Override
        protected Void doInBackground(String... args) {
            try
            {
                List<Valute> response = FetcherApi.getXML2FromUrl(args[0]);
                if (response.size() > 0){
                    ApplicationState.getInstance().setState(Identifier.IS_FIRST_LAUNCH, false);
                }else return null;

                for (Valute valute : response){
                    if (type == OperationType.INSERT) FetcherApi.getInstance().repository.insert(valute);
                    if (type == OperationType.UPDATE) FetcherApi.getInstance().repository.update(valute);
                }
            }
            catch (Exception e)
            {
                Logger.getInstance().push(e.getMessage());
            }

            return null;
        }
    }
}
