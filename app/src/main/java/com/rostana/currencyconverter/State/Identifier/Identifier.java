package com.rostana.currencyconverter.State.Identifier;

public enum Identifier {
    IS_FIRST_LAUNCH,
    VALUE_INPUT,
    SELECTED_VALUTE,
    ACTIVE_CURRENCY
}
