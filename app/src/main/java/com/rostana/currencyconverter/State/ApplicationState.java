package com.rostana.currencyconverter.State;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import com.rostana.currencyconverter.Config.Logger;
import com.rostana.currencyconverter.State.Identifier.Identifier;
import java.util.Map;
import static android.content.Context.MODE_PRIVATE;

public class ApplicationState {

    @SuppressLint("StaticFieldLeak")
    private static final ApplicationState ourInstance = new ApplicationState();

    public static ApplicationState getInstance() {
        return ourInstance;
    }

    private ApplicationState() {}

    private SharedPreferences preferences;
    private Context context;
    public void Init(final Context _context) {
        context = _context;
    }

    public void setState(final Identifier id, final Object content){
        if (content == null) return;

        preferences = context.getSharedPreferences(id.name(), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = context.getSharedPreferences(id.name(), MODE_PRIVATE).edit();

        if (content instanceof Boolean){
            editor.putBoolean(id.name(), (boolean) content);
        }else if (content instanceof Float || content instanceof Double){
            float value = Float.valueOf(String.valueOf(content));
            editor.putFloat(id.name(), value);
        }else if (content instanceof Integer){
            editor.putInt(id.name(), (int) content);
        }else if (content instanceof Long){
            editor.putLong(id.name(), (long) content);
        }else if (content instanceof String){
            editor.putString(id.name(), (String) content);
        }
        editor.apply();
        preferences = null;
    }

    public boolean has(final Identifier id){
        if (context == null) return false;
        preferences = context.getSharedPreferences(id.name(), Context.MODE_PRIVATE);
        Map<String, ?> collection = preferences.getAll();
        preferences = null;
        if (collection.isEmpty()) return false;
        for (Map.Entry item: collection.entrySet()) if (item.getKey().equals(id.name()))return true;
        return false;
    }

    public Object get(final Identifier id){
        if (context == null) return null;
        preferences = context.getSharedPreferences(id.name(), Context.MODE_PRIVATE);
        Map<String, ?> collection = preferences.getAll();
        preferences = null;
        for (Map.Entry item: collection.entrySet()) if (item.getKey().equals(id.name())) return item.getValue();
        return null;
    }

}
