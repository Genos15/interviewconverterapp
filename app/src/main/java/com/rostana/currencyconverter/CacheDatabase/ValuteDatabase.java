package com.rostana.currencyconverter.CacheDatabase;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import com.rostana.currencyconverter.Dao.ValuteDao;
import com.rostana.currencyconverter.Model.Valute;

@Database(entities = Valute.class, version = 1)
public abstract class ValuteDatabase extends RoomDatabase {

    private static ValuteDatabase instance;
    public abstract ValuteDao valuteDao();

    public static synchronized ValuteDatabase getInstance(Context context){
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(), ValuteDatabase.class, "valute_database")
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return instance;
    }

}
