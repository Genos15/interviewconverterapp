package com.rostana.currencyconverter.Repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;
import com.rostana.currencyconverter.CacheDatabase.ValuteDatabase;
import com.rostana.currencyconverter.Dao.ValuteDao;
import com.rostana.currencyconverter.Model.Valute;
import java.util.List;


public class ValuteRepository {

    private ValuteDao valuteDao;
    private LiveData<List<Valute>> allValutes;

    public ValuteRepository(Application application){
        ValuteDatabase database = ValuteDatabase.getInstance(application);
        valuteDao = database.valuteDao();
        allValutes = valuteDao.getAllValutes();
    }

    public void insert(Valute valute){
        new ValuteSubAsyncTask(valuteDao, OperationType.INSERT).execute(valute);
    }

    public void update(Valute valute){
        new ValuteSubAsyncTask(valuteDao, OperationType.UPDATE).execute(valute);
    }

    public void delete(Valute valute){
        new ValuteSubAsyncTask(valuteDao, OperationType.DELETE).execute(valute);
    }

    public void deleteAll(){
        new ValuteSubAsyncTask(valuteDao, OperationType.DELETE_ALL).execute();
    }

    public LiveData<List<Valute>> getAllValutes(){
        return allValutes;
    }

    private static class ValuteSubAsyncTask extends AsyncTask<Valute, Void, Void>{

        private ValuteDao valuteDao;
        private OperationType type;

        private ValuteSubAsyncTask(ValuteDao valuteDao, OperationType type){
            this.valuteDao = valuteDao;
            this.type = type;
        }

        @Override
        protected Void doInBackground(Valute... valutes) {
            try {
                switch (type){
                    case INSERT:{
                        valuteDao.insert(valutes[0]);
                        break;
                    }
                    case UPDATE:{
                        valuteDao.update(valutes[0]);
                        break;
                    }
                    case DELETE:{
                        valuteDao.delete(valutes[0]);
                        break;
                    }
                    case DELETE_ALL:{
                        valuteDao.deleteAllValutes();
                        break;
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }

            return null;
        }
    }
}
