package com.rostana.currencyconverter.Repository;

public enum OperationType {
    INSERT,
    UPDATE,
    DELETE,
    DELETE_ALL,
}
