package com.rostana.currencyconverter.Config;

import android.content.Context;
import android.util.Log;

public class Logger {

    private Context context;
    private static final Logger ourInstance = new Logger();

    public static Logger getInstance() {
        return ourInstance;
    }

    private Logger() {}

    public void Init(Context context){
        this.context = context;
    }

    public void push(String... args){
        if (context != null && args.length > 0){
            StringBuilder buffer = new StringBuilder();
            for (String s : args){
                buffer.append(s).append(" -> ");
            }
            Log.e(context.getPackageName(), buffer.toString());
        }
    }
}
