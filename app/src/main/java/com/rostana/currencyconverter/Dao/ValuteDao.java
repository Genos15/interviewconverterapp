package com.rostana.currencyconverter.Dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.rostana.currencyconverter.Model.Valute;

import java.util.List;

@Dao
public interface ValuteDao {

    @Insert
    void insert(Valute valute);

    @Update
    void update(Valute valute);

    @Delete
    void delete(Valute valute);

    @Query("DELETE FROM valute_table")
    void deleteAllValutes();

    @Query("SELECT * FROM valute_table ORDER BY CharCode ASC")
    LiveData<List<Valute>> getAllValutes();
}
