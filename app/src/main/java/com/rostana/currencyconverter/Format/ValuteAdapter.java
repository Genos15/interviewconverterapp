package com.rostana.currencyconverter.Format;


import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import com.rostana.currencyconverter.Model.Valute;
import com.rostana.currencyconverter.R;
import com.rostana.currencyconverter.State.ApplicationState;
import com.rostana.currencyconverter.State.Identifier.Identifier;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ValuteAdapter extends RecyclerView.Adapter<ValuteAdapter.ValuteHolder> implements Filterable {

    private List<Valute> valutes = new ArrayList<>();
    private List<Valute> valutesAll = new ArrayList<>(valutes);
    private OnValuteItemClick mListener;
    private int selected_position = 0;
    private Context context;

    @NonNull
    @Override
    public ValuteHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();

        if (ApplicationState.getInstance().has(Identifier.ACTIVE_CURRENCY)) {
            String currency = ApplicationState.getInstance().get(Identifier.ACTIVE_CURRENCY).toString();
            for (int i = 0; i < valutes.size(); i++) {
                if (valutes.get(i).getCharCode().equalsIgnoreCase(currency)) {
                    selected_position = i;
                    break;
                }
            }
        }

        View itemView = LayoutInflater.from(context).inflate(R.layout.valute_item, parent, false);
        return new ValuteHolder(itemView, mListener);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ValuteHolder holder, int position) {
        Valute current = valutes.get(position);
        holder.itemView.setBackground(ContextCompat.getDrawable(context, R.drawable.item_valute));
        String active_currency = ApplicationState.getInstance().has(Identifier.ACTIVE_CURRENCY) ?
                        ApplicationState.getInstance().get(Identifier.ACTIVE_CURRENCY).toString() :
                        "";

        if (selected_position == position && valutes.get(position).getCharCode().toLowerCase().equalsIgnoreCase(active_currency.toLowerCase())){
            holder.itemView.setBackground(ContextCompat.getDrawable(context, R.drawable.selected_valute_item));
        }

        holder.text_charcode.setText(current.getCharCode());

        if (!ApplicationState.getInstance().has(Identifier.SELECTED_VALUTE)){
            holder.text_logicalValue.setText("");
            holder.text_value.setText("");
            return;
        }

        double value_valute = Double.valueOf(current.getValue().replace(",", "."));
        double value_selected = Double.valueOf(ApplicationState.getInstance().get(Identifier.SELECTED_VALUTE).toString().replace(",", "."));
        double print_value = round((value_selected / value_valute));
        double value_input = Double.valueOf(ApplicationState.getInstance().get(Identifier.VALUE_INPUT).toString().replace(",", "."));
        double logical = value_input * print_value;


        if (logical <= 0){
            holder.text_value.setText("~");
            return;
        }

        logical = round(logical);
        holder.text_logicalValue.setText(String.valueOf(logical));
        logical = value_input / logical;
        logical = round(logical);
        holder.text_value.setText("1 " + current.getCharCode() + " = " + logical + " " + active_currency);
        holder.text_name.setText(current.getName() !=null ? current.getName() : " ");

    }

    private double round(double value) {
        if (value > 0) {
            String format = String.format(Locale.getDefault(),"%.4f", value);
            return Double.valueOf(format);
        }
        return value;
    }

    @Override
    public int getItemCount() {
        return valutes.size();
    }


    public void setValutes(List<Valute> _valutes) {
        if (this.valutesAll.isEmpty()) valutes = _valutes;
        this.valutesAll = _valutes;
        notifyDataSetChanged();
    }

    @Override
    public Filter getFilter() {
        return valutesFilter;
    }


    private Filter valutesFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            List<Valute> filteredList = new ArrayList<>();
            if (charSequence == null || charSequence.length() == 0) {
                filteredList.addAll(valutesAll);
            } else {
                String filterPattern = charSequence.toString().toLowerCase().trim();
                for (Valute valute : valutesAll) {
                    if (valute.getCharCode().toLowerCase().contains(filterPattern)) {
                        filteredList.add(valute);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            valutes.clear();
            valutes.addAll((List<Valute>) filterResults.values);
            notifyDataSetChanged();
        }
    };

    public class ValuteHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView text_charcode, text_name, text_logicalValue, text_value;

        ValuteHolder(@NonNull View itemView, OnValuteItemClick listener) {
            super(itemView);
            text_charcode = itemView.findViewById(R.id.text_charcode);
            text_name = itemView.findViewById(R.id.text_name);
            text_logicalValue = itemView.findViewById(R.id.text_logicalValue);
            text_value = itemView.findViewById(R.id.text_value);

            itemView.setOnClickListener(view -> {
                onClick(view);
                if (listener != null) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        listener.onItemClick(position);
                    }
                }
            });
        }

        @Override
        public void onClick(View view) {
            if (getAdapterPosition() == RecyclerView.NO_POSITION) return;
            notifyItemChanged(selected_position);
            selected_position = getAdapterPosition();
            notifyItemChanged(selected_position);
            String value = valutes.get(selected_position).getValue();
            ApplicationState.getInstance().setState(Identifier.SELECTED_VALUTE, value);
        }
    }


    public interface OnValuteItemClick {
        void onItemClick(int position);
    }

    public void setOnValuteItemClickListener(OnValuteItemClick listener) {
        mListener = listener;
    }
}